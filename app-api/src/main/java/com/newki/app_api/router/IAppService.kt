package com.newki.app_api.router

import com.alibaba.android.arouter.facade.template.IProvider
import com.newki.app_api.entity.AndroidVersion

interface IAppService : IProvider {
    fun getPushTokenId(): String
    fun getAppVersion(): AndroidVersion
}
