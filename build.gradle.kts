buildscript {

    repositories {
        maven("https://maven.aliyun.com/nexus/content/groups/public/")
        maven("https://maven.aliyun.com/nexus/content/repositories/jcenter")
        maven("https://maven.aliyun.com/nexus/content/repositories/google")
        maven("https://maven.aliyun.com/nexus/content/repositories/gradle-plugin")
        maven("https://jitpack.io")
        google()
        mavenCentral()
        jcenter()
    }

    dependencies {
        classpath("com.android.tools.build:gradle:8.1.3")
        //其他插件依赖
        classpath(VersionAndroidX.Hilt.hiltPlugin)
        classpath(VersionThirdPart.ARouter.plugin)
    }

}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

