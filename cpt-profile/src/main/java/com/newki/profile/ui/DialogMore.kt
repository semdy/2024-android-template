package com.newki.profile.ui

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.newki.profile.R

class DialogMore : BottomSheetDialogFragment() {
    private var height : Int = 0

    fun newInstance(): DialogMore {
        return DialogMore()
    }

    fun setDialogHeight(height: Int): DialogMore {
        this.height = height
        return this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.StyleBottomSheetDialogBg)
    }

    override fun onStart() {
        super.onStart()
        //拿到系统的 bottom_sheet
        val bottomSheetDialog = (dialog as BottomSheetDialog?)!!
        val view =
            bottomSheetDialog.delegate.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)!!
        val behavior = BottomSheetBehavior.from(view)
        //设置弹出高度
        behavior.peekHeight = height
        view.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        behavior.isHideable = false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        val view = LayoutInflater.from(context).inflate(R.layout.layout_item_dialog_more, null)
        dialog.setContentView(view)
        view.findViewById<ImageView>(R.id.vDownClose).setOnClickListener {
            dismiss()
        }
        return dialog
    }

}