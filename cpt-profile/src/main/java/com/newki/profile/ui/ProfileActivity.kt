package com.newki.profile.ui

import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.lifecycleScope
import com.alibaba.android.arouter.facade.annotation.Route
import com.android.basiclib.base.activity.BaseVVDLoadingActivity
import com.android.basiclib.engine.toast.toast
import com.android.basiclib.ext.click
import com.android.basiclib.ext.commContext
import com.android.basiclib.ext.gotoActivity
import com.android.basiclib.view.gloading.GLoadingTitleStatus
import com.android.basiclib.view.gloading.Gloading
import com.android.basiclib.view.gloading.GloadingLoadingAdapter
import com.android.cs_service.ARouterPath
import com.newki.profile.databinding.ActivityProfileBinding
import com.newki.profile.mvi.eis.ArticleUiState
import com.newki.profile.mvi.eis.BannerUiState
import com.newki.profile.mvi.eis.ProfileEffect
import com.newki.profile.mvi.eis.ProfileIntent
import com.newki.profile.mvi.vm.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

/**
 * 用户信息页面
 */
@Route(path = ARouterPath.PATH_PAGE_PROFILE)
@AndroidEntryPoint
class ProfileActivity : BaseVVDLoadingActivity<ProfileViewModel, ActivityProfileBinding>() {
    private var dialogHeight : Int = 0

    companion object {
        fun startInstance() {
            commContext().gotoActivity<ProfileActivity>()
        }
    }

    override fun generateGLoading(): Gloading.Holder {
//        GloadingGlobalAdapter()   默认效果：上下跳动的小球
//        GloadingLoadingAdapter()  普通的菊花Loading
//        GloadingRoatingAdapter()  三层圆圈转动
//        GloadingPlaceHolderlAdapter()  占位图
        return Gloading.from(GloadingLoadingAdapter()).wrap(
            this, GLoadingTitleStatus(isShowTitle = true) //让出Title的位置
        ).withRetry {
            onGoadingRetry()
        }
    }

    override fun startObserve() {
        super.startObserve()

        //分开监听所有的状态
        lifecycleScope.launch {
            mViewModel.uiStateFlow
                .map { it.bannerUiState }
                .distinctUntilChanged()
                .collect { state ->
                    when (state) {
                        is BannerUiState.INIT -> {}
                        is BannerUiState.SUCCESS -> {
                            toast(state.banner.toString())
                        }
                    }
                }
        }

        lifecycleScope.launch {
            mViewModel.uiStateFlow
                .map { it.articleUiState }
                .distinctUntilChanged()
                .collect { state ->
                    when (state) {
                        is ArticleUiState.INIT -> {}
                        is ArticleUiState.SUCCESS -> {
                            toast(state.article.toString())
                        }
                    }
                }
        }

        //效果的SharedFlow监听
        lifecycleScope.launch {
            mViewModel.uiEffectFlow
                .collect {
                    when (it) {
                        is ProfileEffect.ToastArticle -> {
                            toast(it.msg)
                        }
                    }
                }
        }

    }

//    override fun onWindowFocusChanged(hasFocus: Boolean) {
//        super.onWindowFocusChanged(hasFocus)
//        // 获取dialog的高度
//
//        dialogHeight = vllSize.measuredHeight
//        // 获取dialog的高度
//        Log.d( "MainActivity" ,"height = $dialogHeight")
//    }

    override fun init(savedInstanceState: Bundle?) {

        mViewModel.sendUiIntent(ProfileIntent.FetchArticle)

        mBinding.btnProfile.click {
            mViewModel.sendUiIntent(ProfileIntent.FetchArticle)
        }

        mBinding.btnBanner.click {
            //这里使用 WanAndroid - Banner 的数据用于测试
            mViewModel.sendUiIntent(ProfileIntent.FetchBanner)
        }

        mBinding.buttonShow.click {
            MyBottomSheetDialog.show(this)
        }

        mBinding.buttonShow2.click {
            val dialog  = DialogMore().newInstance()
                .setDialogHeight(2000)
            val ft: FragmentTransaction =
                supportFragmentManager.beginTransaction()
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            dialog.show(ft, "DialogMore")
        }
    }


}