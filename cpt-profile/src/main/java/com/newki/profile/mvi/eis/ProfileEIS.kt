package com.newki.profile.mvi.eis

import com.android.basiclib.base.mvi.IUIEffect
import com.android.basiclib.base.mvi.IUiIntent
import com.android.basiclib.base.mvi.IUiState
import com.newki.profile_api.entity.Banner
import com.newki.profile_api.entity.TopArticleBean

//Effect
sealed class ProfileEffect : IUIEffect {
    data class ToastArticle(val msg: String?) : ProfileEffect()
}

//Intent
sealed class ProfileIntent : IUiIntent {
    object FetchArticle : ProfileIntent()
    object FetchBanner : ProfileIntent()
}

//State
data class ProfileState(val bannerUiState: BannerUiState, val articleUiState: ArticleUiState) : IUiState

sealed class BannerUiState {
    object INIT : BannerUiState()
    data class SUCCESS(val banner: List<Banner>) : BannerUiState()
}

sealed class ArticleUiState {
    object INIT : ArticleUiState()
    data class SUCCESS(val article: List<TopArticleBean>) : ArticleUiState()
}