package com.newki.auth.ui

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.launcher.ARouter
import com.android.basiclib.base.activity.BaseVVDActivity
import com.android.basiclib.engine.toast.toast
import com.android.basiclib.engine.toast.toastSuccess
import com.android.basiclib.ext.click
import com.android.basiclib.ext.commContext
import com.android.basiclib.ext.gotoActivity
import com.android.basiclib.utils.log.MyLogUtils
import com.android.basiclib.utils.result.SAF
import com.android.basiclib.utils.result.saf
import com.android.cs_service.ARouterPath
import com.google.gson.Gson
import com.newki.app_api.router.AppServiceProvider
import com.newki.auth.databinding.ActivityAuthLoginBinding
import com.newki.auth.mvi.eis.AuthEffect
import com.newki.auth.mvi.eis.AuthIntent
import com.newki.auth.mvi.eis.AuthUiState
import com.newki.auth.mvi.vm.LoginViewModel
import com.newki.auth_api.router.AuthServiceProvider
import com.newki.profile_api.router.ProfileServiceProvider
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * AUth用户登录页面
 */
@Route(path = ARouterPath.PATH_PAGE_AUTH_LOGIN)
@AndroidEntryPoint
class AuthLoginActivity : BaseVVDActivity<LoginViewModel, ActivityAuthLoginBinding>(), saf by SAF() {

    @Inject
    lateinit var activityGson: Gson

    companion object {
        fun startInstance() {
            commContext().gotoActivity<AuthLoginActivity>()
        }
    }

    override fun startObserve() {
        super.startObserve()

        // UIState的监听
        lifecycleScope.launch {
            mViewModel.uiStateFlow
                .collect { state ->
                    when (state) {
                        is AuthUiState.INIT -> {}
                        is AuthUiState.SUCCESS -> {
                            toast(state.article.toString())
                        }
                    }
                }
        }

        //效果的SharedFlow监听
        lifecycleScope.launch {
            mViewModel.uiEffectFlow
                .collect {
                    when (it) {
                        is AuthEffect.ToastMessage -> {
                            toast(it.msg)
                        }
                    }
                }
        }
    }

    override fun init(savedInstanceState: Bundle?) {

        initLauncher()

        //测试Gson解析
        mViewModel.sendUiIntent(AuthIntent.TestGson(activityGson))


        mBinding.btnLogin.click {
            AuthServiceProvider.authService?.doUserLogin()
        }

        mBinding.btnGotoProfile.click {
            ARouter.getInstance().build(ARouterPath.PATH_PAGE_PROFILE).navigation()
        }

        mBinding.btnVersion.click {
            val version = AppServiceProvider.appService?.getAppVersion()
            toast("version:${version.toString()}")
        }

        //跨模块调用 Profile 组件的接口
        mBinding.btnProfile.click {
            mViewModel.sendUiIntent(AuthIntent.FetchArticle)
        }

    }

}