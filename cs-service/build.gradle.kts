plugins {
    id("com.android.library")
}

// 使用自定义插件
apply<DefaultGradlePlugin>()

android {
    namespace = "com.android.cs_service"
}

dependencies {
    api(project(":cs-baselib"))
}