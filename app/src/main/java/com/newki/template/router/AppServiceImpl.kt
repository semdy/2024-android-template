package com.newki.template.router

import android.content.Context
import com.alibaba.android.arouter.facade.annotation.Route
import com.android.cs_service.ARouterPath
import com.newki.app_api.entity.AndroidVersion
import com.newki.app_api.router.IAppService


@Route(path = ARouterPath.PATH_SERVICE_APP, name = "App模块路由服务")
class AppComponentServiceImpl : IAppService {
    override fun getPushTokenId(): String {
        return "12345678ab"
    }
    override fun getAppVersion(): AndroidVersion {
        return AndroidVersion(code = "1.0.0", url = "http://www.baidu.com")
    }
    override fun init(context: Context?) {

    }
}