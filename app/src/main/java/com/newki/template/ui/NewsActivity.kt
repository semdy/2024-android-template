package com.newki.template.ui

import android.os.Bundle
import com.android.basiclib.adapter.ViewPagerFragmentAdapter
import com.android.basiclib.base.activity.BaseVVDActivity
import com.android.basiclib.base.vm.EmptyViewModel
import com.android.basiclib.ext.commContext
import com.android.basiclib.ext.gotoActivity
import com.newki.template.databinding.ActivityNewsBinding
import com.newki.template.ui.fargment.Load1Fragment
import com.newki.template.ui.fargment.Load2Fragment
import com.newki.template.ui.fargment.Load3Fragment

/**
 * 测试 ViewPager 中 Fragment 的 ViewBinding 逻辑
 */
class NewsActivity : BaseVVDActivity<EmptyViewModel, ActivityNewsBinding>() {

    private val fragments = mutableListOf(Load1Fragment.obtainFragment(), Load2Fragment.obtainFragment(), Load3Fragment.obtainFragment());
    private val titles = mutableListOf("Demo1", "Demo2", "Demo3");
    private val adapter = ViewPagerFragmentAdapter(supportFragmentManager, fragments, titles)

    companion object {
        fun startInstance() {
            commContext().gotoActivity<NewsActivity>()
        }
    }

    override fun init(savedInstanceState: Bundle?) {

        initPager()
    }

    private fun initPager() {
        //默认的添加数据适配器
        mBinding.viewPager.adapter = adapter
        mBinding.viewPager.offscreenPageLimit = fragments.size - 1

        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager)
    }

}